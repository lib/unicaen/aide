<?php

namespace UnicaenAide\Form\Glossaire\Definition;

trait DefinitionFormAwareTrait {

    private DefinitionForm $definitionForm;

    public function getDefinitionForm(): DefinitionForm
    {
        return $this->definitionForm;
    }

    public function setDefinitionForm(DefinitionForm $definitionForm): void
    {
        $this->definitionForm = $definitionForm;
    }

}