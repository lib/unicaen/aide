<?php

namespace UnicaenAide\Form\Glossaire\Definition;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenAide\Service\Glossaire\Definition\DefinitionService;

class DefinitionFormFactory {

    /**
     * @param ContainerInterface $container
     * @return DefinitionForm
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : DefinitionForm
    {
        /**
         * @var DefinitionService $definitionService
         * @var DefinitionHydrator $hydrator
         */
        $definitionService = $container->get(DefinitionService::class);
        $hydrator = $container->get('HydratorManager')->get(DefinitionHydrator::class);

        $form = new DefinitionForm();
        $form->setDefinitionService($definitionService);
        $form->setHydrator($hydrator);
        return $form;
    }
}