<?php

namespace UnicaenAide\Form\Glossaire\Definition;

use Interop\Container\ContainerInterface;

class DefinitionHydratorFactory {

    /**
     * @param ContainerInterface $container
     * @return DefinitionHydrator
     */
    public function __invoke(ContainerInterface $container) : DefinitionHydrator
    {
        $hydrator = new DefinitionHydrator();
        return $hydrator;
    }
}