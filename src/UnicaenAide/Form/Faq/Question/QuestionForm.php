<?php

namespace UnicaenAide\Form\Faq\Question;

use Laminas\Form\Element\Button;
use Laminas\Form\Element\Number;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;

class QuestionForm extends Form {

    public function init() {

        //question
        $this->add([
            'type' => Text::class,
            'name' => 'question',
            'options' => [
                'label' => "Question * :",
            ],
            'attributes' => [
                'id' => 'question',
            ],
        ]);
        //reponse
        $this->add([
            'type' => Textarea::class,
            'name' => 'description',
            'options' => [
                'label' => "Réponse * :",
            ],
            'attributes' => [
                'id' => 'description',
                'class' => "type2",
            ],
        ]);
        //ordre
        $this->add([
            'type' => Number::class,
            'name' => 'ordre',
            'options' => [
                'label' => "Ordre de la question :",
            ],
            'attributes' => [
                'id' => 'ordre',
            ],
        ]);
        //bouton
        $this->add([
            'type' => Button::class,
            'name' => 'creer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);
        //inputfilter
        $this->setInputFilter((new Factory())->createInputFilter([
            'question' => [ 'required' => true, ],
            'description' => [ 'required' => true, ],
            'ordre' => [ 'required' => false, ],
        ]));
    }
}