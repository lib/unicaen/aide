<?php

namespace UnicaenAide\Form\Faq\Question;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class QuestionFormFactory
{
    /**
     * @param ContainerInterface $container
     * @return QuestionForm
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : QuestionForm
    {
        /** @var QuestionHydrator $hydrator */
        $hydrator = $container->get('HydratorManager')->get(QuestionHydrator::class);

        $form = new QuestionForm();
        $form->setHydrator($hydrator);
        return $form;
    }
}