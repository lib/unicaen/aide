<?php

namespace UnicaenAide\Form\Faq\Question;

trait QuestionFormAwareTrait {

    private QuestionForm $questionForm;

    public function getQuestionForm(): QuestionForm
    {
        return $this->questionForm;
    }

    public function setQuestionForm(QuestionForm $questionForm): void
    {
        $this->questionForm = $questionForm;
    }


}