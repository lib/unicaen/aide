<?php

namespace UnicaenAide\Form\Faq\Question;

use Laminas\Hydrator\HydratorInterface;
use UnicaenAide\Entity\Db\FaqQuestion;

class QuestionHydrator implements HydratorInterface {

    /**
     * @param FaqQuestion $object
     * @return array
     */
    public function extract(object $object): array
    {
        $data = [
            'question' => $object->getQuestion(),
            'description' => $object->getReponse(),
            'ordre' => $object->getOrdre(),
        ];
        return $data;
    }

    /**
     * @param array $data
     * @param FaqQuestion $object
     * @return FaqQuestion
     */
    public function hydrate(array $data, object $object)
    {
        $question = (isset($data['question']) AND trim($data['question'])!=="")?trim($data['question']):null;
        $reponse = (isset($data['description']) AND trim($data['description'])!=="")?trim($data['description']):null;
        $ordre = (isset($data['ordre']) AND trim($data['ordre'])!=="")?trim($data['ordre']):null;

        $object->setQuestion($question);
        $object->setReponse($reponse);
        $object->setOrdre($ordre);
        return $object;
    }

}