<?php

namespace UnicaenAide\Form\Faq\Question;

use Psr\Container\ContainerInterface;

class QuestionHydratorFactory {

    /**
     * @param ContainerInterface $container
     * @return QuestionHydrator
     */
    public function __invoke(ContainerInterface $container) : QuestionHydrator
    {
        $hydrator = new QuestionHydrator();
        return $hydrator;
    }
}