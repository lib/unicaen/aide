<?php

namespace UnicaenAide\Form\Documentation\Lien;

use Psr\Container\ContainerInterface;

class LienHydratorFactory {

    public function __invoke(ContainerInterface $container) : LienHydrator
    {
        $hydrator = new LienHydrator();
        return $hydrator;
    }
}