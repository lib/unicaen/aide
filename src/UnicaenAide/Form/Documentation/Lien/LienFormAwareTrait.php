<?php

namespace UnicaenAide\Form\Documentation\Lien;

trait LienFormAwareTrait {

    private LienForm $lienForm;

    public function getLienForm(): LienForm
    {
        return $this->lienForm;
    }

    public function setLienForm(LienForm $lienForm): void
    {
        $this->lienForm = $lienForm;
    }

}