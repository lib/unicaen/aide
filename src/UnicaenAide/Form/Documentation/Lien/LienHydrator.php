<?php

namespace UnicaenAide\Form\Documentation\Lien;

use Laminas\Hydrator\HydratorInterface;
use UnicaenAide\Entity\Db\DocumentationLien;

class LienHydrator implements HydratorInterface
{
    /**
     * @param DocumentationLien $object
     * @return array
     */
    public function extract(object $object): array
    {
        $data = [
            'texte' => $object->getTexte(),
            'lien_texte' => $object->getLienTexte(),
            'lien_url' => $object->getLienUrl(),
            'description' => $object->getDescription(),
            'ordre' => $object->getOrdre(),
            'roles' => ($object->getRoles())??[],
        ];
        return $data;
    }

    /**
     * @param array $data
     * @param DocumentationLien $object
     * @return DocumentationLien
     */
    public function hydrate(array $data, object $object)
    {
        $texte = (isset($data['texte']) AND trim($data['texte'])!=="")?trim($data['texte']):null;
        $lienTexte = (isset($data['lien_texte']) AND trim($data['lien_texte'])!=="")?trim($data['lien_texte']):null;
        $lienUrl = (isset($data['lien_url']) AND trim($data['lien_url'])!=="")?trim($data['lien_url']):null;
        $description = (isset($data['description']) AND trim($data['description'])!=="")?trim($data['description']):null;
        $ordre = (isset($data['ordre']) AND trim($data['ordre'])!=="")?trim($data['ordre']):null;
        $roles = (isset($data['roles']))?implode(';', $data['roles']):null;


        $object->setTexte($texte);
        $object->setLienTexte($lienTexte);
        $object->setLienUrl($lienUrl);
        $object->setDescription($description);
        $object->setOrdre($ordre);
        $object->setRoles($roles);

        return $object;
    }

}