<?php

namespace UnicaenAide\Form\Documentation\Lien;

use Laminas\Form\Element\Button;
use Laminas\Form\Element\Number;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use UnicaenUtilisateur\Service\Role\RoleServiceAwareTrait;

class LienForm extends Form {
    use RoleServiceAwareTrait;

    public function init()
    {
        //texte
        $this->add([
            'type' => Text::class,
            'name' => 'texte',
            'options' => [
                'label' => "Texte avant le lien  :",
            ],
            'attributes' => [
                'id' => 'question',
            ],
        ]);
        //lien_texte
        $this->add([
            'type' => Text::class,
            'name' => 'lien_texte',
            'options' => [
                'label' => "Texte du lien * :",
            ],
            'attributes' => [
                'id' => 'lien_texte',
            ],
        ]);
        //lien_url
        $this->add([
            'type' => Text::class,
            'name' => 'lien_url',
            'options' => [
                'label' => "Adresse du lien * :",
            ],
            'attributes' => [
                'id' => 'question',
            ],
        ]);
        //description
        $this->add([
            'type' => Textarea::class,
            'name' => 'description',
            'options' => [
                'label' => "Description  :",
            ],
            'attributes' => [
                'id' => 'description',
                'class' => "type2",
            ],
        ]);
        //ordre
        $this->add([
            'type' => Number::class,
            'name' => 'ordre',
            'options' => [
                'label' => "Ordre du lien :",
            ],
            'attributes' => [
                'id' => 'ordre',
            ],
        ]);

        //roles
        $this->add([
            'name' => 'roles',
            'type' => Select::class,
            'options' => [
                'label' => "Roles <span class='icon icon-information' title='Laissez vide si tous les rôles peuvent y accéder'></span>: ",
                'label_options' => [ 'disable_html_escape' => true, ],
                'label_attributes' => [
                    'class' => 'control-label',
                ],
                'empty_option' => "Sélection un ou plusieurs rôles ... ",
                'value_options' => $this->getRoleService()->getRolesAsOptions(),
            ],
            'attributes' => [
                'id'                => 'roles',
                'class'             => 'bootstrap-selectpicker show-tick',
                'data-live-search'  => 'true',
                'multiple'          => 'multiple',
            ]
        ]);

        //bouton
        $this->add([
            'type' => Button::class,
            'name' => 'creer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);
        //inputfilter
        $this->setInputFilter((new Factory())->createInputFilter([
            'texte' => [ 'required' => false, ],
            'lien_texte' => [ 'required' => true, ],
            'lien_url' => [ 'required' => true, ],
            'description' => [ 'required' => false, ],
            'ordre' => [ 'required' => false, ],
        ]));
    }
}