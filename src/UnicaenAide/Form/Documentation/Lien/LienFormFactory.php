<?php

namespace UnicaenAide\Form\Documentation\Lien;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenUtilisateur\Service\Role\RoleService;

class LienFormFactory {

    /**
     * @param ContainerInterface $container
     * @return LienForm
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : LienForm
    {
        /** @var RoleService $roleService */
        $roleService = $container->get(RoleService::class);

        /** @var LienHydrator $hydrator */
        $hydrator = $container->get('HydratorManager')->get(LienHydrator::class);

        $form = new LienForm();
        $form->setHydrator($hydrator);
        $form->setRoleService($roleService);
        return $form;
    }
}