<?php

namespace UnicaenAide\Service\Faq\Question;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class QuestionServiceFactory {

    /**
     * @param ContainerInterface $container
     * @return QuestionService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : QuestionService
    {
        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $service = new QuestionService();
        $service->setEntityManager($entityManager);
        return $service;
    }
}