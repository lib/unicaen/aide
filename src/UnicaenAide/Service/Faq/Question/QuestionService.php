<?php

namespace UnicaenAide\Service\Faq\Question;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\QueryBuilder;
use Laminas\Mvc\Controller\AbstractActionController;
use UnicaenAide\Entity\Db\FaqQuestion;
use UnicaenApp\Exception\RuntimeException;
use UnicaenApp\Service\EntityManagerAwareTrait;

class QuestionService {
    use EntityManagerAwareTrait;

    /** GESTION DES ENTITES  ******************************************************************************************/

    /**
     * @param FaqQuestion $question
     * @return FaqQuestion
     */
    public function create(FaqQuestion $question) : FaqQuestion
    {
        try {
            $this->getEntityManager()->persist($question);
            $this->getEntityManager()->flush($question);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.", $e);
        }
        return $question;
    }

    /**
     * @param FaqQuestion $question
     * @return FaqQuestion
     */
    public function update(FaqQuestion $question) : FaqQuestion
    {
        try {
            $this->getEntityManager()->flush($question);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.", $e);
        }
        return $question;
    }

    /**
     * @param FaqQuestion $question
     * @return FaqQuestion
     */
    public function historise(FaqQuestion $question) : FaqQuestion
    {
        try {
            $question->setHistorisee();
            $this->getEntityManager()->flush($question);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.", $e);
        }
        return $question;
    }

    /**
     * @param FaqQuestion $question
     * @return FaqQuestion
     */
    public function restore(FaqQuestion $question) : FaqQuestion
    {
        try {
            $question->setHistorisee(false);
            $this->getEntityManager()->flush($question);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.", $e);
        }
        return $question;
    }

    /**
     * @param FaqQuestion $question
     * @return FaqQuestion
     */
    public function delete(FaqQuestion $question) : FaqQuestion
    {
        try {
            $this->getEntityManager()->remove($question);
            $this->getEntityManager()->flush($question);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.", $e);
        }
        return $question;
    }

    /** REQUETAGE *****************************************************************************************************/

    public function createQueryBuilder() : QueryBuilder
    {
        $qb = $this->getEntityManager()->getRepository(FaqQuestion::class)->createQueryBuilder('question')
            ;
        return $qb;

    }

    /**
     * @param bool $histo
     * @param string $champ
     * @param string $ordre
     * @return FaqQuestion[]
     */
    public function getQuestions(bool $histo = true, string $champ = 'ordre', string $ordre = 'ASC') : array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('question.'.$champ, $ordre);

        if ($histo === false) $qb = $qb->andWhere('question.historisee = :false')->setParameter('false', false);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param int|null $id
     * @return FaqQuestion|null
     */
    public function getQuestion(?int $id) : ?FaqQuestion
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('question.id = :id')->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Question partagent le même id [".$id."]", 0, $e);
        }
        return $result;
    }

    public function getRequestedQuestion(AbstractActionController $controller, string $param = 'question') : ?FaqQuestion
    {
        $id = $controller->params()->fromRoute($param);
        $result = $this->getQuestion($id);
        return $result;
    }

}