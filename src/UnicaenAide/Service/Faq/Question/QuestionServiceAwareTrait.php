<?php

namespace UnicaenAide\Service\Faq\Question;

trait QuestionServiceAwareTrait {

    private QuestionService $questionService;

    /**
     * @return QuestionService
     */
    public function getQuestionService(): QuestionService
    {
        return $this->questionService;
    }

    /**
     * @param QuestionService $questionService
     */
    public function setQuestionService(QuestionService $questionService): void
    {
        $this->questionService = $questionService;
    }

}