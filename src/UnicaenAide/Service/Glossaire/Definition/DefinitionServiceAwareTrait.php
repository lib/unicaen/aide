<?php

namespace UnicaenAide\Service\Glossaire\Definition;


trait DefinitionServiceAwareTrait {

    private DefinitionService $definitionService;

    public function getDefinitionService(): DefinitionService
    {
        return $this->definitionService;
    }

    public function setDefinitionService(DefinitionService $definitionService): void
    {
        $this->definitionService = $definitionService;
    }

}