<?php

namespace UnicaenAide\Service\Glossaire\Definition;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class DefinitionServiceFactory {

    /**
     * @param ContainerInterface $container
     * @return DefinitionService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : DefinitionService
    {
        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $service = new DefinitionService();
        $service->setEntityManager($entityManager);
        return $service;
    }
}