<?php

namespace UnicaenAide\Service\Glossaire\Definition;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\QueryBuilder;
use UnicaenApp\Exception\RuntimeException;
use UnicaenApp\Service\EntityManagerAwareTrait;
use UnicaenAide\Entity\Db\GlossaireDefinition;
use Laminas\Mvc\Controller\AbstractActionController;

class DefinitionService {
    use EntityManagerAwareTrait;

    /** Gestion des entités *******************************************************************************************/

    /**
     * @param GlossaireDefinition $definition
     * @return GlossaireDefinition
     */
    public function create(GlossaireDefinition $definition) : GlossaireDefinition
    {
        try {
            $this->getEntityManager()->persist($definition);
            $this->getEntityManager()->flush($definition);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.", $e);
        }
        return $definition;
    }

    /**
     * @param GlossaireDefinition $definition
     * @return GlossaireDefinition
     */
    public function update(GlossaireDefinition $definition) : GlossaireDefinition
    {
        try {
            $this->getEntityManager()->flush($definition);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.", $e);
        }
        return $definition;
    }

    /**
     * @param GlossaireDefinition $definition
     * @return GlossaireDefinition
     */
    public function historise(GlossaireDefinition $definition) : GlossaireDefinition
    {
        try {
            $definition->setHistorisee(true);
            $this->getEntityManager()->flush($definition);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.", $e);
        }
        return $definition;
    }

    /**
     * @param GlossaireDefinition $definition
     * @return GlossaireDefinition
     */
    public function restore(GlossaireDefinition $definition) : GlossaireDefinition
    {
        try {
            $definition->setHistorisee(false);
            $this->getEntityManager()->flush($definition);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.", $e);
        }
        return $definition;
    }

    /**
     * @param GlossaireDefinition $definition
     * @return GlossaireDefinition
     */
    public function delete(GlossaireDefinition $definition) : GlossaireDefinition
    {
        try {
            $this->getEntityManager()->remove($definition);
            $this->getEntityManager()->flush($definition);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.", $e);
        }
        return $definition;
    }

    /** REQUETAGE *****************************************************************************************************/

    /**
     * @return QueryBuilder
     */
    public function createQueryBuilder() : QueryBuilder
    {
        $qb = $this->getEntityManager()->getRepository(GlossaireDefinition::class)->createQueryBuilder('definition');
        return $qb;
    }

    /**
     * @param string $champ
     * @param string $ordre
     * @return GlossaireDefinition[]
     */
    public function getDefinitions(string $champ = 'terme', string $ordre = 'ASC') : array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('definition.' . $champ, $ordre);
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param integer $id
     * @return GlossaireDefinition|null
     */
    public function getDefinition(int $id) : ?GlossaireDefinition
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('definition.id = :id')
            ->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Definition partagent le même id [".$id."]",0,$e);
        }
        return $result;
    }

    /**
     * @param string $terme
     * @return GlossaireDefinition|null
     */
    public function getDefinitionByTerme(string $terme) : ?GlossaireDefinition
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('definition.terme = :terme')
            ->setParameter('terme', $terme);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Definition partagent le même terme [".$terme."]",0,$e);
        }
        return $result;
    }

    /**
     * @param AbstractActionController $controller
     * @param string $param
     * @return GlossaireDefinition|null
     */
    public function getRequestedDefinition(AbstractActionController $controller, string $param='definition') : ?GlossaireDefinition
    {
        $id = $controller->params()->fromRoute($param);
        return $this->getDefinition($id);
    }

}