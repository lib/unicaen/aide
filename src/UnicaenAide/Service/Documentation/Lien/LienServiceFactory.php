<?php

namespace UnicaenAide\Service\Documentation\Lien;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class LienServiceFactory {

    /**
     * @param ContainerInterface $container
     * @return LienService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : LienService
    {
        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $service = new LienService();
        $service->setEntityManager($entityManager);
        return $service;
    }
}