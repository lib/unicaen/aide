<?php

namespace UnicaenAide\Service\Documentation\Lien;

trait LienServiceAwareTrait
{
    private LienService $lienService;

    public function getLienService(): LienService
    {
        return $this->lienService;
    }

    public function setLienService(LienService $lienService): void
    {
        $this->lienService = $lienService;
    }
}