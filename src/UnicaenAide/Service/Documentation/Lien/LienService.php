<?php

namespace UnicaenAide\Service\Documentation\Lien;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\QueryBuilder;
use Laminas\Mvc\Controller\AbstractActionController;
use UnicaenAide\Entity\Db\DocumentationLien;
use UnicaenApp\Service\EntityManagerAwareTrait;
use ZfcUser\Mapper\Exception\RuntimeException;

class LienService {
    use EntityManagerAwareTrait;

    /** GESTION DES ENTITES  ******************************************************************************************/

    /**
     * @param DocumentationLien $lien
     * @return DocumentationLien
     */
    public function create(DocumentationLien $lien) : DocumentationLien
    {
        try {
            $this->getEntityManager()->persist($lien);
            $this->getEntityManager()->flush($lien);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.", $e);
        }
        return $lien;
    }

    /**
     * @param DocumentationLien $lien
     * @return DocumentationLien
     */
    public function update(DocumentationLien $lien) : DocumentationLien
    {
        try {
            $this->getEntityManager()->flush($lien);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.", $e);
        }
        return $lien;
    }

    /**
     * @param DocumentationLien $lien
     * @return DocumentationLien
     */
    public function historise(DocumentationLien $lien) : DocumentationLien
    {
        try {
            $lien->setHistorisee();
            $this->getEntityManager()->flush($lien);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.", $e);
        }
        return $lien;
    }

    /**
     * @param DocumentationLien $lien
     * @return DocumentationLien
     */
    public function restore(DocumentationLien $lien) : DocumentationLien
    {
        try {
            $lien->setHistorisee(false);
            $this->getEntityManager()->flush($lien);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.", $e);
        }
        return $lien;
    }

    /**
     * @param DocumentationLien $lien
     * @return DocumentationLien
     */
    public function delete(DocumentationLien $lien) : DocumentationLien
    {
        try {
            $this->getEntityManager()->remove($lien);
            $this->getEntityManager()->flush($lien);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue lors de l'enregistrement en BD.", $e);
        }
        return $lien;
    }

    /** REQUETAGE *****************************************************************************************************/

    public function createQueryBuilder() : QueryBuilder
    {
        $qb = $this->getEntityManager()->getRepository(DocumentationLien::class)->createQueryBuilder('lien')
        ;
        return $qb;

    }

    /**
     * @param bool $histo
     * @param string $champ
     * @param string $ordre
     * @return DocumentationLien[]
     */
    public function getLiens(bool $histo = true, string $champ = 'ordre', string $ordre = 'ASC') : array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('lien.'.$champ, $ordre);

        if ($histo === false) $qb = $qb->andWhere('lien.historisee = :false')->setParameter('false', false);

        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /**
     * @param int|null $id
     * @return DocumentationLien|null
     */
    public function getLien(?int $id) : ?DocumentationLien
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('lien.id = :id')->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Lien partagent le même id [".$id."]", 0, $e);
        }
        return $result;
    }

    public function getRequestedLien(AbstractActionController $controller, string $param = 'lien') : ?DocumentationLien
    {
        $id = $controller->params()->fromRoute($param);
        $result = $this->getLien($id);
        return $result;
    }

}