<?php

namespace UnicaenAide\View\Helper;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenAide\Service\Glossaire\Definition\DefinitionService;

class DictionnaireGenerationViewHelperFactory {

    /**
     * @param ContainerInterface $container
     * @return DictionnaireGenerationViewHelper
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : DictionnaireGenerationViewHelper
    {
        /**
         * @var DefinitionService $definitionService
         */
        $definitionService = $container->get(DefinitionService::class);
        $definitions = $definitionService->getDefinitions();

        $helper = new DictionnaireGenerationViewHelper();
        $helper->setDefinitions($definitions);
        return $helper;
    }
}