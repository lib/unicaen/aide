<?php

namespace UnicaenAide\View\Helper;

use Application\View\Renderer\PhpRenderer;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Resolver\TemplatePathStack;
use UnicaenAide\Entity\Db\GlossaireDefinition;

class DictionnaireGenerationViewHelper extends AbstractHelper
{
    /** @var GlossaireDefinition[] */
    private $definitions;

    /**
     * @param GlossaireDefinition[] $definitions
     * @return DictionnaireGenerationViewHelper
     */
    public function setDefinitions(array $definitions): DictionnaireGenerationViewHelper
    {
        $this->definitions = $definitions;
        return $this;
    }

    /**
     * @param array $options
     * @return string|Partial
     */
    public function __invoke($options = [])
    {
        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('dictionnaire', ['definitions' => $this->definitions, 'options' => $options]);
    }
}
