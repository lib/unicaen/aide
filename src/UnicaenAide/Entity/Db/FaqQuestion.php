<?php

namespace UnicaenAide\Entity\Db;


class FaqQuestion
{
    private ?int $id                = null;
    private ?string $question       = null;
    private ?string $reponse        = null;
    private ?int $ordre             = null;
    private ?bool $historisee       = false;

    public function getId(): int
    {
        return $this->id;
    }

    public function getQuestion(): ?string
    {
        return $this->question;
    }

    public function setQuestion(?string $question): void
    {
        $this->question = $question;
    }

    public function getReponse(): ?string
    {
        return $this->reponse;
    }

    public function setReponse(?string $reponse): void
    {
        $this->reponse = $reponse;
    }

    public function getOrdre(): ?int
    {
        return $this->ordre;
    }

    public function setOrdre(?int $ordre): void
    {
        $this->ordre = $ordre;
    }

    public function estHistorise(): bool
    {
        return ($this->historisee === true);
    }

    public function estNonHistorise(): bool
    {
        return ($this->historisee === false);
    }

    public function setHistorisee(bool $historisee = true): void
    {
        $this->historisee = $historisee;
    }

}