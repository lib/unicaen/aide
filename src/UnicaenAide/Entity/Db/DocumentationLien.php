<?php

namespace UnicaenAide\Entity\Db;



class DocumentationLien {

    private ?int $id                = null;
    private ?string $texte          = null;
    private ?string $lienTexte      = null;
    private ?string $lienUrl        = null;
    private ?string $description    = null;
    private ?int $ordre             = null;
    private ?bool $historisee       = false;

    private ?string $roles = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getTexte(): ?string
    {
        return $this->texte;
    }

    public function setTexte(?string $texte): void
    {
        $this->texte = $texte;
    }

    public function getLienTexte(): ?string
    {
        return $this->lienTexte;
    }

    public function setLienTexte(?string $lienTexte): void
    {
        $this->lienTexte = $lienTexte;
    }

    public function getLienUrl(): ?string
    {
        return $this->lienUrl;
    }

    public function setLienUrl(?string $lienUrl): void
    {
        $this->lienUrl = $lienUrl;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getOrdre(): ?int
    {
        return $this->ordre;
    }

    public function setOrdre(?int $ordre): void
    {
        $this->ordre = $ordre;
    }

    public function estHistorise(): bool
    {
        return ($this->historisee === true);
    }

    public function estNonHistorise(): bool
    {
        return ($this->historisee === false);
    }

    public function setHistorisee(bool $historisee = true): void
    {
        $this->historisee = $historisee;
    }


    public function getRoles() : array
    {
        if ($this->roles === null) return [];
        return explode(';', $this->roles);
    }

    public function setRoles(?string $roles)
    {
        $this->roles = $roles;
    }


}