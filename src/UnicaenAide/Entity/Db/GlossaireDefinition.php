<?php

namespace UnicaenAide\Entity\Db;


class GlossaireDefinition {

    private ?int $id                = null;
    private ?string $terme          = null;
    private ?string $definition     = null;
    private ?string $alternatives   = null;
    private ?bool $historisee       = false;

    public function getId(): int
    {
        return $this->id;
    }

    public function getTerme(): ?string
    {
        return $this->terme;
    }

    public function setTerme(?string $terme): void
    {
        $this->terme = $terme;
    }

    public function getDefinition(): ?string
    {
        return $this->definition;
    }

    public function setDefinition(?string $definition): void
    {
        $this->definition = $definition;
    }

    public function getAlternatives(): ?string
    {
        return $this->alternatives;
    }

    public function setAlternatives(?string $alternatives): void
    {
        $this->alternatives = $alternatives;
    }

    public function estHistorise(): bool
    {
        return ($this->historisee === true);
    }

    public function estNonHistorise(): bool
    {
        return ($this->historisee === false);
    }

    public function setHistorisee(bool $historisee = true): void
    {
        $this->historisee = $historisee;
    }

}