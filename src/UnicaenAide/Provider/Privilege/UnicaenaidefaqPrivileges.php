<?php

namespace UnicaenAide\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class UnicaenaidefaqPrivileges extends Privileges
{
    const FAQ_AFFICHER = 'unicaenaidefaq-faq_afficher';
    const FAQ_INDEX = 'unicaenaidefaq-faq_index';
    const FAQ_AJOUTER = 'unicaenaidefaq-faq_ajouter';
    const FAQ_MODIFIER = 'unicaenaidefaq-faq_modifier';
    const FAQ_HISTORISER = 'unicaenaidefaq-faq_historiser';
    const FAQ_SUPPRIMER = 'unicaenaidefaq-faq_supprimer';
}
