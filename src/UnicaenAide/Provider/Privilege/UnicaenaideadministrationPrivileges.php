<?php

namespace UnicaenAide\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class UnicaenaideadministrationPrivileges extends Privileges
{
    const UNICAENAIDEADMINISTRATION_INDEX = 'unicaenaideadministration-unicaenaideadministration_index';
}
