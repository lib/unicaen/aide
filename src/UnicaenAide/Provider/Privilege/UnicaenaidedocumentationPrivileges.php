<?php

namespace UnicaenAide\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class UnicaenaidedocumentationPrivileges extends Privileges
{
    const DOCUMENTATION_AFFICHER = 'unicaenaidedocumentation-documentation_afficher';
    const DOCUMENTATION_INDEX = 'unicaenaidedocumentation-documentation_index';
    const DOCUMENTATION_AJOUTER = 'unicaenaidedocumentation-documentation_ajouter';
    const DOCUMENTATION_MODIFIER = 'unicaenaidedocumentation-documentation_modifier';
    const DOCUMENTATION_HISTORISER = 'unicaenaidedocumentation-documentation_historiser';
    const DOCUMENTATION_SUPPRIMER = 'unicaenaidedocumentation-documentation_supprimer';
}
