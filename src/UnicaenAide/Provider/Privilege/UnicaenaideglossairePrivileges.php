<?php

namespace UnicaenAide\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class UnicaenaideglossairePrivileges extends Privileges
{
    const GLOSSAIRE_AFFICHER = 'unicaenaideglossaire-glossaire_afficher';

    const GLOSSAIRE_INDEX = 'unicaenaideglossaire-glossaire_index';
    const GLOSSAIRE_AJOUTER = 'unicaenaideglossaire-glossaire_ajouter';
    const GLOSSAIRE_MODIFIER = 'unicaenaideglossaire-glossaire_modifier';
    const GLOSSAIRE_HISTORISER = 'unicaenaideglossaire-glossaire_historiser';
    const GLOSSAIRE_SUPPRIMER = 'unicaenaideglossaire-glossaire_supprimer';
}
