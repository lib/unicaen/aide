<?php

namespace UnicaenAide\Controller\Glossaire;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenAide\Form\Glossaire\Definition\DefinitionForm;
use UnicaenAide\Service\Glossaire\Definition\DefinitionService;

class DefinitionControllerFactory {

    /**
     * @param ContainerInterface $container
     * @return DefinitionController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : DefinitionController
    {
        /**
         * @var DefinitionService $definitionService
         */
        $definitionService = $container->get(DefinitionService::class);

        /**
         * @var DefinitionForm $definitionForm
         */
        $definitionForm = $container->get('FormElementManager')->get(DefinitionForm::class);

        $controller = new DefinitionController();
        $controller->setDefinitionService($definitionService);
        $controller->setDefinitionForm($definitionForm);
        return $controller;
    }
}