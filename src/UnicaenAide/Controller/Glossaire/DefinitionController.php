<?php

namespace UnicaenAide\Controller\Glossaire;

use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenAide\Entity\Db\GlossaireDefinition;
use UnicaenAide\Form\Glossaire\Definition\DefinitionFormAwareTrait;
use UnicaenAide\Service\Glossaire\Definition\DefinitionServiceAwareTrait;

class DefinitionController extends AbstractActionController {
    use DefinitionServiceAwareTrait;
    use DefinitionFormAwareTrait;

    public function indexAction() : ViewModel
    {
        $definitions = $this->getDefinitionService()->getDefinitions();

        return new ViewModel([
            'definitions' => $definitions,
        ]);
    }

    public function afficherAction() : ViewModel
    {
        $definition = $this->getDefinitionService()->getRequestedDefinition($this);

        $vm = new ViewModel([
            'title' => "Affichage de la définition du terme [".$definition->getTerme()."]",
            'definition' => $definition,
        ]);
        return $vm;
    }

    public function ajouterAction() : ViewModel
    {
        $definition = new GlossaireDefinition();
        $form = $this->getDefinitionForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-aide/glossaire/definition/ajouter', [], [], true));
        $form->bind($definition);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getDefinitionService()->create($definition);
            }
        }

        $vm = new ViewModel([
            'title' => "Ajouter une définition",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-aide/default/default-form');
        return $vm;
    }

    public function modifierAction() : ViewModel
    {
        $definition = $this->getDefinitionService()->getRequestedDefinition($this);
        $form = $this->getDefinitionForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-aide/glossaire/definition/modifier', ['definition' => $definition->getId()], [], true));
        $form->bind($definition);
        $form->setOldTerme($definition->getTerme());

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getDefinitionService()->update($definition);
            }
        }

        $vm = new ViewModel([
            'title' => "Modification de la définition de [".$definition->getTerme()."]",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-aide/default/default-form');
        return $vm;
    }

    public function historiserAction() : Response
    {
        $definition = $this->getDefinitionService()->getRequestedDefinition($this);
        $this->getDefinitionService()->historise($definition);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('unicaen-aide/glossaire/definition', [], [], true);
    }

    public function restaurerAction() : Response
    {
        $definition = $this->getDefinitionService()->getRequestedDefinition($this);
        $this->getDefinitionService()->restore($definition);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('unicaen-aide/glossaire/definition', [], [], true);
    }

    public function supprimerAction()
    {
        $definition = $this->getDefinitionService()->getRequestedDefinition($this);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getDefinitionService()->delete($definition);
            exit();
        }

        $vm = new ViewModel();
        if ($definition !== null) {
            $vm->setTemplate('unicaen-aide/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression de la définition " . $definition->getTerme(),
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('unicaen-aide/glossaire/definition/supprimer', ["definition" => $definition->getId()], [], true),
            ]);
        }
        return $vm;
    }
}