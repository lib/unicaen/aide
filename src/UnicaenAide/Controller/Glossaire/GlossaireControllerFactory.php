<?php

namespace UnicaenAide\Controller\Glossaire;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenAide\Service\Glossaire\Definition\DefinitionService;

class GlossaireControllerFactory {

    /**
     * @param ContainerInterface $container
     * @return GlossaireController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : GlossaireController
    {
        /**
         * @var DefinitionService $definitionService
         */
        $definitionService = $container->get(DefinitionService::class);

        $controller = new GlossaireController();
        $controller->setDefinitionService($definitionService);
        return $controller;
    }
}