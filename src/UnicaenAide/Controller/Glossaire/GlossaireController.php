<?php

namespace UnicaenAide\Controller\Glossaire;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenAide\Service\Glossaire\Definition\DefinitionServiceAwareTrait;

class GlossaireController extends AbstractActionController {
    use DefinitionServiceAwareTrait;

    public function indexAction() : ViewModel
    {
        $definitions = $this->getDefinitionService()->getDefinitions();

        $vm = new ViewModel([
            'definitions' => $definitions,
        ]);
        $vm->setTemplate('unicaen-aide/glossaire/afficher');
        return $vm;
    }
}