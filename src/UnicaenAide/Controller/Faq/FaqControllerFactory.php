<?php

namespace UnicaenAide\Controller\Faq;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenAide\Service\Faq\Question\QuestionService;

class FaqControllerFactory {

    /**
     * @param ContainerInterface $container
     * @return FaqController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : FaqController
    {
        /**
         * @var QuestionService $questionService
         */
        $questionService = $container->get(QuestionService::class);

        $controller = new FaqController();
        $controller->setQuestionService($questionService);
        return $controller;
    }
}