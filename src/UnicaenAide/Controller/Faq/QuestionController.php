<?php

namespace UnicaenAide\Controller\Faq;

use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenAide\Entity\Db\FaqQuestion;
use UnicaenAide\Form\Faq\Question\QuestionFormAwareTrait;
use UnicaenAide\Service\Faq\Question\QuestionServiceAwareTrait;

class QuestionController extends AbstractActionController {
    use QuestionServiceAwareTrait;
    use QuestionFormAwareTrait;

    public function indexAction() : ViewModel
    {
        $questions = $this->getQuestionService()->getQuestions();

        return new ViewModel([
            'questions' => $questions,
        ]);
    }

    public function afficherAction() : ViewModel
    {
        $question = $this->getQuestionService()->getRequestedQuestion($this);

        return new ViewModel([
            'title' => "Affichage de la question",
            'question' => $question,
        ]);
    }

    public function ajouterAction() : ViewModel
    {
        $question = new FaqQuestion();
        $form = $this->getQuestionForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-aide/faq/question/ajouter', [], [], true));
        $form->bind($question);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getQuestionService()->create($question);
            }
        }

        $vm = new ViewModel([
            'title' => "Ajouter une question",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-aide/default/default-form');
        return $vm;
    }

    public function modifierAction() : ViewModel
    {
        $question = $this->getQuestionService()->getRequestedQuestion($this);
        $form = $this->getQuestionForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-aide/faq/question/modifier', ['question' => $question->getId()], [], true));
        $form->bind($question);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getQuestionService()->update($question);
            }
        }

        $vm = new ViewModel([
            'title' => "Modification de la qestion de la question",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-aide/default/default-form');
        return $vm;
    }

    public function historiserAction() : Response
    {
        $question = $this->getQuestionService()->getRequestedQuestion($this);
        $this->getQuestionService()->historise($question);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('unicaen-aide/faq/question', [], [], true);
    }

    public function restaurerAction() : Response
    {
        $question = $this->getQuestionService()->getRequestedQuestion($this);
        $this->getQuestionService()->restore($question);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('unicaen-aide/faq/question', [], [], true);
    }

    public function supprimerAction()
    {
        $question = $this->getQuestionService()->getRequestedQuestion($this);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getQuestionService()->delete($question);
            exit();
        }

        $vm = new ViewModel();
        if ($question !== null) {
            $vm->setTemplate('unicaen-aide/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression de la question ",
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('unicaen-aide/faq/question/supprimer', ["question" => $question->getId()], [], true),
            ]);
        }
        return $vm;
    }
}

