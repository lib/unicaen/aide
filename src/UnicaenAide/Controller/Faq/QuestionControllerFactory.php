<?php

namespace UnicaenAide\Controller\Faq;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenAide\Form\Faq\Question\QuestionForm;
use UnicaenAide\Service\Faq\Question\QuestionService;

class QuestionControllerFactory
{
    /**
     * @param ContainerInterface $container
     * @return QuestionController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : QuestionController
    {
        /**
         * @var QuestionService $questionService
         */
        $questionService = $container->get(QuestionService::class);

        /**
         * @var QuestionForm $questionForm
         */
        $questionForm = $container->get('FormElementManager')->get(QuestionForm::class);

        $controller = new QuestionController();
        $controller->setQuestionService($questionService);
        $controller->setQuestionForm($questionForm);
        return $controller;
    }
}

