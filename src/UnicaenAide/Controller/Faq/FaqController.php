<?php

namespace UnicaenAide\Controller\Faq;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenAide\Service\Faq\Question\QuestionServiceAwareTrait;

class FaqController extends AbstractActionController {
    use QuestionServiceAwareTrait;

    public function indexAction() : ViewModel
    {
        $questions = $this->getQuestionService()->getQuestions(false);

        $vm = new ViewModel([
            'questions' => $questions,
        ]);
        $vm->setTemplate('unicaen-aide/faq/afficher');
        return $vm;
    }
}