<?php

namespace UnicaenAide\Controller;


use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class AdministrationController extends AbstractActionController
{
    public function indexAction() : ViewModel
    {
        return new ViewModel([]);
    }
}

?>