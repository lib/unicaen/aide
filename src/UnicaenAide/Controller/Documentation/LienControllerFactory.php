<?php

namespace UnicaenAide\Controller\Documentation;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenAide\Form\Documentation\Lien\LienForm;
use UnicaenAide\Service\Documentation\Lien\LienService;
use UnicaenUtilisateur\Service\Role\RoleService;

class LienControllerFactory {

    /**
     * @param ContainerInterface $container
     * @return LienController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : LienController
    {
        /**
         * @var LienService $lienService
         * @var RoleService $roleService
         * @var LienForm $lienForm
         */
        $lienService = $container->get(LienService::class);
        $roleService = $container->get(RoleService::class);
        $lienForm = $container->get('FormElementManager')->get(LienForm::class);

        $controller = new LienController();
        $controller->setLienService($lienService);
        $controller->setRoleService($roleService);
        $controller->setLienForm($lienForm);
        return $controller;
    }
}