<?php

namespace UnicaenAide\Controller\Documentation;

use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenAide\Entity\Db\DocumentationLien;
use UnicaenAide\Form\Documentation\Lien\LienFormAwareTrait;
use UnicaenAide\Service\Documentation\Lien\LienServiceAwareTrait;
use UnicaenUtilisateur\Service\Role\RoleServiceAwareTrait;

class LienController extends AbstractActionController {
    use LienServiceAwareTrait;
    use RoleServiceAwareTrait;
    use LienFormAwareTrait;

    public function indexAction() : ViewModel
    {
        $liens = $this->getLienService()->getLiens();
        $roles_ = $this->getRoleService()->getRepo()->findAll();
        $roles = [];
        foreach ($roles_ as $role) {
            $roles[$role->getId()] = $role;
        }

        return new ViewModel([
            'liens' => $liens,
            'roles' => $roles,
        ]);
    }

    public function afficherAction() : ViewModel
    {
        $lien = $this->getLienService()->getRequestedLien($this);
        
        $roles = [];
        if (!empty($lien->getRoles()))
        {
            foreach ($lien->getRoles() as $roleId) {
                $role = $this->getRoleService()->find($roleId);
                if ($role) $roles[] = $role;
            }
        }

        return new ViewModel([
            'title' => "Affichage de la lien",
            'lien' => $lien,
            'roles' => $roles,
        ]);
    }

    public function ajouterAction() : ViewModel
    {
        $lien = new DocumentationLien();
        $form = $this->getLienForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-aide/documentation/lien/ajouter', [], [], true));
        $form->bind($lien);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getLienService()->create($lien);
            }
        }

        $vm = new ViewModel([
            'title' => "Ajouter une documentation",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-aide/default/default-form');
        return $vm;
    }

    public function modifierAction() : ViewModel
    {
        $lien = $this->getLienService()->getRequestedLien($this);
        $form = $this->getLienForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-aide/documentation/lien/modifier', ['lien' => $lien->getId()], [], true));
        $form->bind($lien);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getLienService()->update($lien);
            }
        }

        $vm = new ViewModel([
            'title' => "Modification d'une documentation",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-aide/default/default-form');
        return $vm;
    }

    public function historiserAction() : Response
    {
        $lien = $this->getLienService()->getRequestedLien($this);
        $this->getLienService()->historise($lien);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('unicaen-aide/documentation/lien', [], [], true);
    }

    public function restaurerAction() : Response
    {
        $lien = $this->getLienService()->getRequestedLien($this);
        $this->getLienService()->restore($lien);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('unicaen-aide/documentation/lien', [], [], true);
    }

    public function supprimerAction()
    {
        $lien = $this->getLienService()->getRequestedLien($this);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getLienService()->delete($lien);
            exit();
        }

        $vm = new ViewModel();
        if ($lien !== null) {
            $vm->setTemplate('unicaen-aide/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression de la documentation",
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('unicaen-aide/documentation/lien/supprimer', ["lien" => $lien->getId()], [], true),
            ]);
        }
        return $vm;
    }

}