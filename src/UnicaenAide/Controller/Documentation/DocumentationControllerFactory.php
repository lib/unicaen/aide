<?php

namespace UnicaenAide\Controller\Documentation;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenAide\Service\Documentation\Lien\LienService;
use UnicaenPrivilege\Service\Privilege\PrivilegeService;
use UnicaenUtilisateur\Service\User\UserService;

class DocumentationControllerFactory {

    /**
     * @param ContainerInterface $container
     * @return DocumentationController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : DocumentationController
    {
        /**
         * @var LienService $lienService
         * @var PrivilegeService $privilegeService
         * @var UserService $userService
         */
        $lienService = $container->get(LienService::class);
        $userService = $container->get(UserService::class);

        $controller = new DocumentationController();
        $controller->setLienService($lienService);
        $controller->setUserService($userService);
        return $controller;
    }
}