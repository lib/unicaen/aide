<?php

namespace UnicaenAide\Controller\Documentation;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenAide\Service\Documentation\Lien\LienServiceAwareTrait;
use UnicaenPrivilege\Service\Privilege\PrivilegeServiceAwareTrait;
use UnicaenUtilisateur\Service\User\UserServiceAwareTrait;

class DocumentationController extends AbstractActionController
{
    use LienServiceAwareTrait;
    use UserServiceAwareTrait;

    public function indexAction() : ViewModel
    {
        $liens = $this->getLienService()->getLiens(false);
        $vm =  new ViewModel([
            'liens' => $liens,
            'role' => $this->getUserService()->getConnectedRole(),
        ]);
        $vm->setTemplate('unicaen-aide/documentation/afficher');
        return $vm;
    }
}