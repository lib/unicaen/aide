<?php

namespace UnicaenAide\Controller;

use Psr\Container\ContainerInterface;

class AdministrationControllerFactory {

    public function __invoke(ContainerInterface $container) : AdministrationController
    {
        $controller = new AdministrationController();
        return $controller;
    }
}