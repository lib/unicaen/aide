-- CREATION DES TABLES --

create table unicaen_aide_glossaire_definition
(
    id           serial
        constraint unicaen_glossaire_definition_pk_ primary key,
    terme        varchar(1024)         not null,
    definition   text                  not null,
    alternatives text,
    historisee   boolean default false not null
);
create unique index unicaen_glossaire_definition_id_uindex_ on unicaen_aide_glossaire_definition (id);
create unique index unicaen_glossaire_definition_terme_uindex_ on unicaen_aide_glossaire_definition (terme);

create table unicaen_aide_faq_question
(
    id         serial
        constraint unicaen_faq_question_pk primary key,
    question   varchar(4096) not null,
    reponse    text          not null,
    historisee boolean default false,
    ordre      integer
);
create unique index unicaen_faq_question_id_uindex on unicaen_aide_faq_question (id);

create table unicaen_aide_documentation_lien
(
    id          serial
        constraint unicaen_aide_documentation_lien_pk primary key,
    texte       varchar(1024),
    lien_texte  varchar(1024) not null,
    lien_url    varchar(1024) not null,
    description text,
    ordre       integer,
    historisee  boolean default false,
    role_ids    varchar(4096)
);
create unique index unicaen_aide_documentation_lien_id_uindex on unicaen_aide_documentation_lien (id);

-- PRIVILEGES --

INSERT INTO unicaen_privilege_categorie (code, libelle, ordre, namespace)
VALUES ('unicaenaideglossaire', 'UnicaenAide - Glossaire', 100300, 'UnicaenAide\Provider\Privilege');
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'glossaire_afficher', 'Affichage du glossaire', 10 UNION
    SELECT 'glossaire_index', 'Accès à l''index des défintions', 100 UNION
    SELECT 'glossaire_ajouter', 'Ajouter une définition', 110 UNION
    SELECT 'glossaire_modifier', 'Modifier une définition', 120 UNION
    SELECT 'glossaire_historiser', 'Historiser/restaurer une définition', 130 UNION
    SELECT 'glossaire_supprimer', 'Supprimer une supprimer', 140
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'unicaenaideglossaire';

INSERT INTO unicaen_privilege_categorie (code, libelle, ordre, namespace)
VALUES ('unicaenaidefaq', 'UnicaenAide - F.A.Q.', 100200, 'UnicaenAide\Provider\Privilege');
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'faq_afficher', 'Afficher la FAQ', 10 UNION
    SELECT 'faq_index', 'Accès à l''index des questions', 20 UNION
    SELECT 'faq_ajouter', 'Ajouter une question', 30 UNION
    SELECT 'faq_modifier', 'Modifier une question', 40 UNION
    SELECT 'faq_historiser', 'Historiser/restaurer une question', 50 UNION
    SELECT 'faq_supprimer', 'Supprimer une question', 60
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'unicaenaidefaq';

INSERT INTO unicaen_privilege_categorie (code, libelle, ordre, namespace)
VALUES ('unicaenaidedocumentation', 'UnicaenAide - Documentation', 100400, 'UnicaenAide\Provider\Privilege');
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'documentation_afficher', 'Afficher la documentation', 10 UNION
    SELECT 'documentation_index', 'Accès à l''index des documentations', 20 UNION
    SELECT 'documentation_ajouter', 'Ajouter une documentation', 30 UNION
    SELECT 'documentation_modifier', 'Modifier une documentation', 40 UNION
    SELECT 'documentation_historiser', 'Historiser/restaurer une documentation', 50 UNION
    SELECT 'documentation_supprimer', 'Supprimer une documentation', 60
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'unicaenaidedocumentation';
