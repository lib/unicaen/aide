UnicaenAide
============

UnicaenAide met à disposition une FAQ, un glossaire et une zone de dépôt de documentation. 
Cette bibliothèque propose aussi une zone de gestion de ces documentations.

Installation et dépendance
--------------------------

UnicaenAide est packagé sous le nom `unicaen/aide`.
UnicaenAide dépend des blibliothèques UnicaenPrivilege et de UnicaenUtilisateur.

Dans le script de création des tables et d'insertion des privilèges est disponible ici : [doc/database.sql]

Utilisation
-----------

Une fois installée et les privilèges accordés, on retourve un menu **Aide** dans le header du site et un menu **Administrion_de_l'aide** dans le menu d'administration.


Changelog
=========

0.0.3 (30/05/2023)
----

- Mise à jour des routes pour transformer unicaen-aide en aide (ce qui gênait certains établissements)
- Mise à jour des scripts de BD (table et privilège)
