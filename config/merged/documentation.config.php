<?php

namespace UnicaenParametre;

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenAide\Controller\Documentation\DocumentationController;
use UnicaenAide\Controller\Documentation\DocumentationControllerFactory;
use UnicaenAide\Controller\Documentation\LienController;
use UnicaenAide\Controller\Documentation\LienControllerFactory;
use UnicaenAide\Form\Documentation\Lien\LienForm;
use UnicaenAide\Form\Documentation\Lien\LienFormFactory;
use UnicaenAide\Form\Documentation\Lien\LienHydrator;
use UnicaenAide\Form\Documentation\Lien\LienHydratorFactory;
use UnicaenAide\Provider\Privilege\UnicaenaidedocumentationPrivileges;
use UnicaenAide\Service\Documentation\Lien\LienService;
use UnicaenAide\Service\Documentation\Lien\LienServiceFactory;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => DocumentationController::class,
                    'action' => [
                        'index',
                    ],
                    'pivileges' => UnicaenaidedocumentationPrivileges::DOCUMENTATION_AFFICHER,
                ],
                [
                    'controller' => LienController::class,
                    'action' => [
                        'index',
                    ],
                    'pivileges' => UnicaenaidedocumentationPrivileges::DOCUMENTATION_INDEX,
                ],
                [
                    'controller' => LienController::class,
                    'action' => [
                        'afficher',
                    ],
                    'pivileges' => UnicaenaidedocumentationPrivileges::DOCUMENTATION_INDEX,
                ],
                [
                    'controller' => LienController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'pivileges' => UnicaenaidedocumentationPrivileges::DOCUMENTATION_AJOUTER,
                ],
                [
                    'controller' => LienController::class,
                    'action' => [
                        'modifier',
                    ],
                    'pivileges' => UnicaenaidedocumentationPrivileges::DOCUMENTATION_MODIFIER,
                ],
                [
                    'controller' => LienController::class,
                    'action' => [
                        'historiser',
                        'restaurer',
                    ],
                    'pivileges' => UnicaenaidedocumentationPrivileges::DOCUMENTATION_HISTORISER,
                ],
                [
                    'controller' => LienController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'pivileges' => UnicaenaidedocumentationPrivileges::DOCUMENTATION_SUPPRIMER,
                ],
            ],
        ],
    ],

    'navigation'      => [
        'default' => [
            'home' => [
                'pages' => [
                    'unicaenaide' => [
                        'pages' => [
                            'documentation' => [
                                'label'    => "Documentation",
                                'route'    => "unicaen-aide/documentation",
                                'resource' => PrivilegeController::getResourceId(DocumentationController::class, 'index'),
                                'order'    => 300,
                                'pages' => [],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'unicaen-aide' => [
                'type'  => Literal::class,
                'options' => [
                    'route'    => '/aide',
                ],
                'child_routes' => [
                    'documentation' => [
                        'type'  => Literal::class,
                        'options' => [
                            'route'    => '/documentation',
                            'defaults' => [
                                'controller' => DocumentationController::class,
                                'action' => 'index'
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'lien' => [
                                'type'  => Literal::class,
                                'options' => [
                                    'route'    => '/lien',
                                    'defaults' => [
                                        'controller' => LienController::class,
                                        'action' => 'index'
                                    ],
                                ],
                                'may_terminate' => true,
                                'child_routes' => [
                                    'afficher' => [
                                        'type'  => Segment::class,
                                        'options' => [
                                            'route'    => '/afficher/:lien',
                                            'defaults' => [
                                                'controller' => LienController::class,
                                                'action' => 'afficher'
                                            ],
                                        ],
                                    ],
                                    'ajouter' => [
                                        'type'  => Literal::class,
                                        'options' => [
                                            'route'    => '/ajouter',
                                            'defaults' => [
                                                'controller' => LienController::class,
                                                'action' => 'ajouter'
                                            ],
                                        ],
                                    ],
                                    'modifier' => [
                                        'type'  => Segment::class,
                                        'options' => [
                                            'route'    => '/modifier/:lien',
                                            'defaults' => [
                                                'controller' => LienController::class,
                                                'action' => 'modifier'
                                            ],
                                        ],
                                    ],
                                    'historiser' => [
                                        'type'  => Segment::class,
                                        'options' => [
                                            'route'    => '/historiser/:lien',
                                            'defaults' => [
                                                'controller' => LienController::class,
                                                'action' => 'historiser'
                                            ],
                                        ],
                                    ],
                                    'restaurer' => [
                                        'type'  => Segment::class,
                                        'options' => [
                                            'route'    => '/restaurer/:lien',
                                            'defaults' => [
                                                'controller' => LienController::class,
                                                'action' => 'restaurer'
                                            ],
                                        ],
                                    ],
                                    'supprimer' => [
                                        'type'  => Segment::class,
                                        'options' => [
                                            'route'    => '/supprimer/:lien',
                                            'defaults' => [
                                                'controller' => LienController::class,
                                                'action' => 'supprimer'
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ]
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            LienService::class => LienServiceFactory::class,
        ],
    ],
    'controllers'     => [
        'factories' => [
            DocumentationController::class => DocumentationControllerFactory::class,
            LienController::class => LienControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            LienForm::class => LienFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            LienHydrator::class => LienHydratorFactory::class,
        ],
    ]

];