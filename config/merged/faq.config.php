<?php

namespace UnicaenParametre;

use UnicaenAide\Controller\Faq\FaqController;
use UnicaenAide\Controller\Faq\FaqControllerFactory;
use UnicaenAide\Controller\Faq\QuestionController;
use UnicaenAide\Controller\Faq\QuestionControllerFactory;
use UnicaenAide\Form\Faq\Question\QuestionForm;
use UnicaenAide\Form\Faq\Question\QuestionFormFactory;
use UnicaenAide\Form\Faq\Question\QuestionHydrator;
use UnicaenAide\Form\Faq\Question\QuestionHydratorFactory;
use UnicaenAide\Provider\Privilege\UnicaenaidefaqPrivileges;
use UnicaenAide\Service\Faq\Question\QuestionService;
use UnicaenAide\Service\Faq\Question\QuestionServiceFactory;
use UnicaenPrivilege\Guard\PrivilegeController;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => FaqController::class,
                    'action' => [
                        'index',
                    ],
                    'pivileges' => UnicaenaidefaqPrivileges::FAQ_AFFICHER,
                ],
                [
                    'controller' => QuestionController::class,
                    'action' => [
                        'index',
                    ],
                    'pivileges' => UnicaenaidefaqPrivileges::FAQ_INDEX,
                ],
                [
                    'controller' => QuestionController::class,
                    'action' => [
                        'afficher',
                    ],
                    'pivileges' => UnicaenaidefaqPrivileges::FAQ_INDEX,
                ],
                [
                    'controller' => QuestionController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'pivileges' => UnicaenaidefaqPrivileges::FAQ_AJOUTER,
                ],
                [
                    'controller' => QuestionController::class,
                    'action' => [
                        'modifier',
                    ],
                    'pivileges' => UnicaenaidefaqPrivileges::FAQ_MODIFIER,
                ],
                [
                    'controller' => QuestionController::class,
                    'action' => [
                        'historiser',
                        'restaurer',
                    ],
                    'pivileges' => UnicaenaidefaqPrivileges::FAQ_HISTORISER,
                ],
                [
                    'controller' => QuestionController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'pivileges' => UnicaenaidefaqPrivileges::FAQ_SUPPRIMER,
                ],
            ],
        ],
    ],

    'navigation'      => [
        'default' => [
            'home' => [
                'pages' => [
                    'unicaenaide' => [
                        'pages' => [
                            'faq' => [
                                'label'    => "Foire aux questions",
                                'route'    => "unicaen-aide/faq",
                                'resource' => PrivilegeController::getResourceId(FaqController::class, 'index'),
                                'order'    => 100,
                                'pages' => [],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'unicaen-aide' => [
                'type'  => Literal::class,
                'options' => [
                    'route'    => '/aide',
                ],
                'child_routes' => [
                    'faq' => [
                        'type'  => Literal::class,
                        'options' => [
                            'route'    => '/faq',
                            'defaults' => [
                                'controller' => FaqController::class,
                                'action' => 'index'
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'question' => [
                                'type'  => Literal::class,
                                'options' => [
                                    'route'    => '/question',
                                    'defaults' => [
                                        'controller' => QuestionController::class,
                                        'action' => 'index'
                                    ],
                                ],
                                'may_terminate' => true,
                                'child_routes' => [
                                    'afficher' => [
                                        'type'  => Segment::class,
                                        'options' => [
                                            'route'    => '/afficher/:question',
                                            'defaults' => [
                                                'controller' => QuestionController::class,
                                                'action' => 'afficher'
                                            ],
                                        ],
                                    ],
                                    'ajouter' => [
                                        'type'  => Literal::class,
                                        'options' => [
                                            'route'    => '/ajouter',
                                            'defaults' => [
                                                'controller' => QuestionController::class,
                                                'action' => 'ajouter'
                                            ],
                                        ],
                                    ],
                                    'modifier' => [
                                        'type'  => Segment::class,
                                        'options' => [
                                            'route'    => '/modifier/:question',
                                            'defaults' => [
                                                'controller' => QuestionController::class,
                                                'action' => 'modifier'
                                            ],
                                        ],
                                    ],
                                    'historiser' => [
                                        'type'  => Segment::class,
                                        'options' => [
                                            'route'    => '/historiser/:question',
                                            'defaults' => [
                                                'controller' => QuestionController::class,
                                                'action' => 'historiser'
                                            ],
                                        ],
                                    ],
                                    'restaurer' => [
                                        'type'  => Segment::class,
                                        'options' => [
                                            'route'    => '/restaurer/:question',
                                            'defaults' => [
                                                'controller' => QuestionController::class,
                                                'action' => 'restaurer'
                                            ],
                                        ],
                                    ],
                                    'supprimer' => [
                                        'type'  => Segment::class,
                                        'options' => [
                                            'route'    => '/supprimer/:question',
                                            'defaults' => [
                                                'controller' => QuestionController::class,
                                                'action' => 'supprimer'
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            QuestionService::class => QuestionServiceFactory::class,
        ],
    ],
    'controllers'     => [
        'factories' => [
            FaqController::class => FaqControllerFactory::class,
            QuestionController::class => QuestionControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            QuestionForm::class => QuestionFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            QuestionHydrator::class => QuestionHydratorFactory::class,
        ],
    ]

];