<?php

namespace UnicaenAide;

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenAide\Controller\Glossaire\DefinitionController;
use UnicaenAide\Controller\Glossaire\DefinitionControllerFactory;
use UnicaenAide\Controller\Glossaire\GlossaireController;
use UnicaenAide\Controller\Glossaire\GlossaireControllerFactory;
use UnicaenAide\Form\Glossaire\Definition\DefinitionForm;
use UnicaenAide\Form\Glossaire\Definition\DefinitionFormFactory;
use UnicaenAide\Form\Glossaire\Definition\DefinitionHydrator;
use UnicaenAide\Form\Glossaire\Definition\DefinitionHydratorFactory;
use UnicaenAide\Provider\Privilege\UnicaenaideglossairePrivileges;
use UnicaenAide\Service\Glossaire\Definition\DefinitionService;
use UnicaenAide\Service\Glossaire\Definition\DefinitionServiceFactory;
use UnicaenAide\View\Helper\DictionnaireGenerationViewHelper;
use UnicaenAide\View\Helper\DictionnaireGenerationViewHelperFactory;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => GlossaireController::class,
                    'action' => [
                        'index',
                    ],
                    'pivileges' => UnicaenaideglossairePrivileges::GLOSSAIRE_AFFICHER,
                ],
                [
                    'controller' => DefinitionController::class,
                    'action' => [
                        'index',
                    ],
                    'pivileges' => UnicaenaideglossairePrivileges::GLOSSAIRE_INDEX,
                ],
                [
                    'controller' => DefinitionController::class,
                    'action' => [
                        'afficher',
                    ],
                    'pivileges' => UnicaenaideglossairePrivileges::GLOSSAIRE_INDEX,
                ],
                [
                    'controller' => DefinitionController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'pivileges' => UnicaenaideglossairePrivileges::GLOSSAIRE_AJOUTER,
                ],
                [
                    'controller' => DefinitionController::class,
                    'action' => [
                        'modifier',
                    ],
                    'pivileges' => UnicaenaideglossairePrivileges::GLOSSAIRE_MODIFIER,
                ],
                [
                    'controller' => DefinitionController::class,
                    'action' => [
                        'historiser',
                        'restaurer',
                    ],
                    'pivileges' => UnicaenaideglossairePrivileges::GLOSSAIRE_HISTORISER,
                ],
                [
                    'controller' => DefinitionController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'pivileges' => UnicaenaideglossairePrivileges::GLOSSAIRE_SUPPRIMER,
                ],
            ],
        ],
    ],

    'navigation'      => [
        'default' => [
            'home' => [
                'pages' => [
                    'unicaenaide' => [
                        'pages' => [
                            'unicaenaide' => [
                                'label'    => "Glossaire",
                                'route'    => "unicaen-aide/glossaire",
                                'resource' => PrivilegeController::getResourceId(GlossaireController::class, 'index'),
                                'order'    => 200,
                                'pages' => [],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'unicaen-aide' => [
                'type'  => Literal::class,
                'options' => [
                    'route'    => '/aide',
                ],
                'child_routes' => [
                    'glossaire' => [
                        'type'  => Literal::class,
                        'options' => [
                            'route'    => '/glossaire',
                            'defaults' => [
                                'controller' => GlossaireController::class,
                                'action' => 'index'
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'definition' => [
                                'type'  => Literal::class,
                                'options' => [
                                    'route'    => '/definition',
                                    'defaults' => [
                                        'controller' => DefinitionController::class,
                                        'action' => 'index'
                                    ],
                                ],
                                'may_terminate' => true,
                                'child_routes' => [
                                    'afficher' => [
                                        'type'  => Segment::class,
                                        'options' => [
                                            'route'    => '/afficher/:definition',
                                            'defaults' => [
                                                'controller' => DefinitionController::class,
                                                'action' => 'afficher'
                                            ],
                                        ],
                                    ],
                                    'ajouter' => [
                                        'type'  => Literal::class,
                                        'options' => [
                                            'route'    => '/ajouter',
                                            'defaults' => [
                                                'controller' => DefinitionController::class,
                                                'action' => 'ajouter'
                                            ],
                                        ],
                                    ],
                                    'modifier' => [
                                        'type'  => Segment::class,
                                        'options' => [
                                            'route'    => '/modifier/:definition',
                                            'defaults' => [
                                                'controller' => DefinitionController::class,
                                                'action' => 'modifier'
                                            ],
                                        ],
                                    ],
                                    'historiser' => [
                                        'type'  => Segment::class,
                                        'options' => [
                                            'route'    => '/historiser/:definition',
                                            'defaults' => [
                                                'controller' => DefinitionController::class,
                                                'action' => 'historiser'
                                            ],
                                        ],
                                    ],
                                    'restaurer' => [
                                        'type'  => Segment::class,
                                        'options' => [
                                            'route'    => '/restaurer/:definition',
                                            'defaults' => [
                                                'controller' => DefinitionController::class,
                                                'action' => 'restaurer'
                                            ],
                                        ],
                                    ],
                                    'supprimer' => [
                                        'type'  => Segment::class,
                                        'options' => [
                                            'route'    => '/supprimer/:definition',
                                            'defaults' => [
                                                'controller' => DefinitionController::class,
                                                'action' => 'supprimer'
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            DefinitionService::class => DefinitionServiceFactory::class,
        ],
    ],
    'controllers'     => [
        'factories' => [
            DefinitionController::class => DefinitionControllerFactory::class,
            GlossaireController::class => GlossaireControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            DefinitionForm::class => DefinitionFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            DefinitionHydrator::class => DefinitionHydratorFactory::class,
        ],
    ],
    'view_helpers' => [
        'factories' => [
            DictionnaireGenerationViewHelper::class => DictionnaireGenerationViewHelperFactory::class,
        ],
        'aliases' => [
            'dictionnaireGeneration' => DictionnaireGenerationViewHelper::class,
        ],
    ],

];