<?php

namespace UnicaenAide;

use Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain;
use Doctrine\ORM\Mapping\Driver\XmlDriver;
use Laminas\Router\Http\Literal;
use UnicaenAide\Controller\AdministrationController;
use UnicaenAide\Controller\IndexController;
use UnicaenAide\Controller\IndexControllerFactory;
use UnicaenAide\Provider\Privilege\UnicaenaideadministrationPrivileges;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => IndexController::class,
                    'action' => [
                        'index',
                    ],
                    'roles' => [],
                ],
            ],
        ],
    ],

    'router'          => [
        'routes' => [
            'unicaen-aide' => [
                'type'  => Literal::class,
                'options' => [
                    'route'    => '/aide',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action' => 'index'
                    ],
                ],
                'may_terminate' => true,
           ],
        ],
    ],

    'navigation'      => [
        'default' => [
            'home' => [
                'pages' => [
                    'unicaenaide' => [
                        'label'    => "Aide",
                        'route'    => "unicaen-aide",
                        'resource' => PrivilegeController::getResourceId(IndexController::class, 'index'),
                        'order'    => 999999,
                        'pages' => [],
                    ],
                ],
            ],
        ],
    ],

    'controllers'     => [
        'factories' => [
            IndexController::class => IndexControllerFactory::class,
        ],
    ],

];