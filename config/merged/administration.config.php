<?php

namespace UnicaenParametre;

use Laminas\Router\Http\Literal;
use UnicaenAide\Controller\AdministrationController;
use UnicaenAide\Controller\AdministrationControllerFactory;
use UnicaenAide\Provider\Privilege\UnicaenaideadministrationPrivileges;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => AdministrationController::class,
                    'action' => [
                        'index',
                    ],
                    'pivileges' => UnicaenaideadministrationPrivileges::UNICAENAIDEADMINISTRATION_INDEX,
                ],
            ],
        ],
    ],

    'navigation'      => [
        'default' => [
            'home' => [
                'pages' => [
                    'administration' => [
                        'pages' => [
                            'unicaenaide' => [
                                'label'    => "Administration de l'aide",
                                'route'    => "unicaen-aide/administration",
                                'resource' => PrivilegeController::getResourceId(AdministrationController::class, 'index'),
                                'order'    => 7080,
                                'pages' => [],
                                'icon' => 'fas fa-angle-right',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'router'          => [
        'routes' => [
            'unicaen-aide' => [
                'type'  => Literal::class,
                'options' => [
                    'route'    => '/aide',
                ],
                'child_routes' => [
                    'administration' => [
                        'type'  => Literal::class,
                        'options' => [
                            'route'    => '/administration',
                            'defaults' => [
                                'controller' => AdministrationController::class,
                                'action' => 'index'
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [],
    ],
    'controllers'     => [
        'factories' => [
            AdministrationController::class => AdministrationControllerFactory::class
        ],
    ],
    'form_elements' => [
        'factories' => [],
    ],
    'hydrators' => [
        'factories' => [],
    ]

];